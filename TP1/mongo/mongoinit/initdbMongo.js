// Connect to MongoDB
conn = new Mongo();

// Create or switch to a database
db = conn.getDB("BDD_Mongo");

// Create a collection
db.createCollection("products");
